package com.ryi.life.service;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by heshun on 15-12-31.
 */
public class MainTest {

    public static void main(String[] args) {
        String hex = DigestUtils.md5Hex("123456");
        System.out.println("e10adc3949ba59abbe56e057f20f883e");
        System.out.println(hex);
    }
}
