package com.ryi.life.service;


import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 所有测试类(若需要回滚)都需要继承该类(BasicTransactionalTest)，该类已和spring进行集成。
 * “@runwith”与spring进行集成。
 * “@ContextConfiguration(locations = "classpath:rock-servlet.xml")”读取spring的相关配置文件。
 *
 * @author ShenYuTing
 * @version 1.0，2013-7-10
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-root.xml"})
public abstract class NoTransactionalTest extends AbstractTransactionalJUnit4SpringContextTests {
}
