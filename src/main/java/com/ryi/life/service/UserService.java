package com.ryi.life.service;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * 用户的service
 * Created by heshun on 15-12-30.
 */
public interface UserService {


    /**
     * shiro的验证
     *
     * @param principalCollection
     * @param name
     * @return
     */
    AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken principalCollection, String name);

    AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection);
}

