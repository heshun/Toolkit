package com.ryi.life.entity;

import java.io.Serializable;

/**
 * Created by heshun on 15-12-30.
 */
public class User implements Serializable {
    private String password;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
