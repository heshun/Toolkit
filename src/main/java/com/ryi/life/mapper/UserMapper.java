package com.ryi.life.mapper;

import com.ryi.life.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * Created by heshun on 15-12-30.
 */
public interface UserMapper {

    /**
     * 根据用户名查找用户
     *
     * @param userName 用户名称
     * @return
     */
    User findByUserName(@Param("userName") String userName);
}
