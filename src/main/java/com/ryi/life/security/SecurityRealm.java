package com.ryi.life.security;

import com.ryi.life.service.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;

/**
 * shiro安全验证框架
 */
public class SecurityRealm extends AuthorizingRealm {

    @Resource
    UserService userService;

    /**
     * 获取登陆用户的权限信息
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return userService.doGetAuthorizationInfo(principalCollection);
    }

    /**
     * 对登陆用户进行验证，判断密码是否正确
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) {
        return userService.doGetAuthenticationInfo(authenticationToken, getName());
    }
}